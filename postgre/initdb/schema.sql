-- public.loan definition

-- Drop table

-- DROP TABLE public.loan;

CREATE TABLE public.loan (
	loan_id uuid NOT NULL,
	"name" varchar NULL,
	amount numeric NULL,
	loan_type int2 NULL,
	CONSTRAINT loan_pk PRIMARY KEY (loan_id),
	CONSTRAINT loan_un UNIQUE (loan_id)
);

-- public.customer definition

-- Drop table

-- DROP TABLE public.customer;

CREATE TABLE public.customer (
	cust_id uuid NOT NULL,
	cust_name varchar NULL,
	address varchar NULL,
	birthday date NULL,
	CONSTRAINT customer_pk PRIMARY KEY (cust_id),
	CONSTRAINT customer_un UNIQUE (cust_id)
);

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
ALTER EXTENSION "uuid-ossp" SET SCHEMA public;