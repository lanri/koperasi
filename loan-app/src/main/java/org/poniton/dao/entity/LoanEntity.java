package org.poniton.dao.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.poniton.dao.dto.LoanDto;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author <a href="mailto:poniton.panjaitan@sigma.co.id">poniton.panjaitan</a>
 */
@Entity
@NoArgsConstructor
@Table(name = "loan")
@Data
public class LoanEntity {
    @Id
    UUID loan_id;
    String name;
    BigDecimal amount;
    int loan_type;

    public LoanDto parseDto(){
        LoanDto dto = new LoanDto();
        dto.setLoan_id(this.loan_id);
        dto.setLoan_type(this.loan_type);
        dto.setName(this.name);
        dto.setAmount(this.amount);
        return dto;
    }

}
