package org.poniton.dao.repo;

import org.poniton.dao.dto.LoanDto;
import org.poniton.dao.entity.LoanEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import java.util.UUID;

/**
 * @author <a href="mailto:poniton.panjaitan@sigma.co.id">poniton.panjaitan</a>
 */
public interface LoanRepo extends JpaRepository<LoanEntity, UUID>, JpaSpecificationExecutor<LoanEntity> {
    @Query(value = "select * from public.loan l "+
            "where l.name = ?1 ", nativeQuery = true)
    LoanEntity findByName(String name);
}
