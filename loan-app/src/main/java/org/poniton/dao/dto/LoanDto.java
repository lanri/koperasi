package org.poniton.dao.dto;

import lombok.Data;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author <a href="mailto:poniton.panjaitan@sigma.co.id">poniton.panjaitan</a>
 */
@Data
public class LoanDto {
    UUID loan_id;
    String name;
    BigDecimal amount;
    int loan_type;
}
