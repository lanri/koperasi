package org.poniton.service;

import org.poniton.dao.dto.LoanDto;
import java.util.List;

/**
 * @author <a href="mailto:poniton.panjaitan@sigma.co.id">poniton.panjaitan</a>
 */
public interface LoanService {
    List<LoanDto> getAllLoan();

    LoanDto getByName(String name);
}
