package org.poniton.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.poniton.dao.dto.LoanDto;
import org.poniton.dao.entity.LoanEntity;
import org.poniton.dao.repo.LoanRepo;
import org.poniton.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:poniton.panjaitan@sigma.co.id">poniton.panjaitan</a>
 */
@Service
@Slf4j
public class LoanImplService implements LoanService {

    @Autowired
    LoanRepo loanRepo;
    @Cacheable("AllLoanData")
    @Override
    public List<LoanDto> getAllLoan() {
        log.info("DB accessed, Cache Not Used");
        long start = System.currentTimeMillis();
        List<LoanEntity> loanEntities = loanRepo.findAll();
        if(!loanEntities.isEmpty()) {
            List<LoanDto> loanDtoList = new ArrayList<>();
            for (LoanEntity loan : loanEntities
            ) {
                loanDtoList.add(loan.parseDto());
            }
            log.info("getAllLoan - Finish Eta : {} ms",(System.currentTimeMillis()-start));
            return loanDtoList;
        }else {
            return null;
        }
    }

    @Override
    public LoanDto getByName(String name) {
        return loanRepo.findByName(name).parseDto();
    }
}
