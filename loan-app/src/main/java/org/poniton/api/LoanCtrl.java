package org.poniton.api;

import org.poniton.dao.dto.LoanDto;
import org.poniton.service.LoanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * @author <a href="mailto:poniton.panjaitan@sigma.co.id">poniton.panjaitan</a>
 */
@RestController
@RequestMapping("/api/loan-app")
public class LoanCtrl {

    // Initializing instance of Logger for Controller
    private static final Logger log = LoggerFactory.getLogger(LoanCtrl.class);
    @Autowired
    LoanService loanService;

    @PostMapping(value="/hallokoperasi")
    public String hellokoperasi(){
      return "Hello Koperasi";
    }

    @PostMapping(value="/retrieve/loan",  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<LoanDto>> getAllLoan() throws Exception{
        log.info("Retrieve Controller");
        try {
            List<LoanDto> loanDtos = loanService.getAllLoan();
            if(!loanDtos.isEmpty())
                return new ResponseEntity<>(loanDtos, HttpStatus.OK);
            else
                throw new Exception("EMPTY");
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }


    }

    @PostMapping(value="/retrieve/loan-by-name",  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoanDto> getLoanByName(@RequestBody LoanDto loan) throws Exception{
        log.info("Retrieve Controller");
        try {
            LoanDto loanDto = loanService.getByName(loan.getName());
            if(loanDto != null)
                return new ResponseEntity<>(loanDto, HttpStatus.OK);
            else
                throw new Exception("EMPTY");
        }catch (Exception e){
            log.error(e.getMessage());
            throw new Exception("ERROR");
        }

    }
}
