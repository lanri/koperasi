package org.poniton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author <a href="mailto:poniton.panjaitan@sigma.co.id">poniton.panjaitan</a>
 */
@SpringBootApplication
public class LoanApp {
    public static void main(String[] args) {
        SpringApplication microService = new SpringApplication(LoanApp.class);
        microService.run(args);
    }
}